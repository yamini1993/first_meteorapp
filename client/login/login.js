if (Meteor.isClient) {
   
Template.login.events({
    'submit form': function(event){
        event.preventDefault();
        var email = $('[id=email]').val();
        var password = $('[id=password]').val();
       //var userObj = Meteor.user();
        //console.log(userObj);

        Meteor.loginWithPassword(email,password,function (error){
          
          if(error){
          console.log(error.reason);
          alert(error.reason);
          Router.go('login');
          }else{
            Router.go('dashboard');
          }
      });
    }
});
}