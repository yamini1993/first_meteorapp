
Template.services.helpers({
  user:function(){
     return Meteor.users.find({},{sort: {"profile.points": -1}});
  },
  selectedClass: function(){
  	 var userid = this._id;
    var selecteduser = Session.get('selecteduser');
	    if(userid == selecteduser){
	        return "selected"
	    } 
	}
   
});

Template.services.events({
	'click .users': function(){
	 	var userid=this._id
	 	Session.set('selecteduser',userid);
      var selecteduser = Session.get('selecteduser');
   		//console.log(selecteduser);
  	},
  	'click .addpoint': function(){
    var selecteduser = Session.get('selecteduser');
     Meteor.call('addPoints',selecteduser, function(){

        });
}
});