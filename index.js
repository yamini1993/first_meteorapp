Router.configure({
    layoutTemplate: 'main'
});
Router.route('/',{
  template:"index"
});
Router.route('/index');
Router.route('/register');
Router.route('/login');
Router.route('/services');
Router.route('/dashboard');
//Tasks = new Mongo.Collection("tasks");
//Register_tbl = new Mongo.Collection("register");

if (Meteor.isClient) {
   
}

 /*Template.dashboard.events({
   'click .logout': function(event){
        event.preventDefault();
        Meteor.logout();
    }
   });*/

/*
    Template.body.helpers({
    tasks: function () {
      return Tasks.find({},{sort: {createdAt: -1}});
      //return Tasks.findone({content:"hi"});
    }
  }); 


  Template.body.events({
    "submit .new-task": function (event) {
      // Prevent default browser form submit
      event.preventDefault();
 
      // Get value from form element
      var text = event.target.text.value;
 
      // Insert a task into the collection
      Tasks.insert({
        text: text,
        createdAt: new Date() // current time
      });
 
      // Clear form
      event.target.text.value = "";
    }
  });

 Template.task.events({
    "click .toggle-checked": function () {
      // Set the checked property to the opposite of its current value
      Tasks.update(this._id, {
        $set: {checked: ! this.checked}
      });
    },
    "click .delete": function () {
      Tasks.remove(this._id);
    }
  });




   // counter starts at 0
  Session.setDefault('counter', 0);

  Template.hello.helpers({
    counter: function () {
      return Session.get('counter');
    }
  });
  Template.example.helpers({
    name: "Yamini"
    
  });

  Template.hello.events({
    'dblclick button': function () {
      // increment the counter when button is clicked
      Session.set('counter', Session.get('counter') + 1);
    }
  });
}
*/
if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
